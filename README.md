**Kinda depricated**

Nextcloud 28 added a lot of Vue.js components alongside lots of material Design elements so this project is now kinda redundant.
Besides a few tweaks, this CSS doesn't add much anymore.

**Compatibility**

Only the newest stable Nextcloud Version is supported.

**Installation**:

To use this Theme you have to have the **[Custom CSS](https://apps.nextcloud.com/apps/theming_customcss)** App for Nextcloud installed. Then login into your Nextcloud as Admin and navigate to /index.php/settings/admin/theming (Settings -> Theming) and Copy&Paste the Content of the `Material-Design-for-Nextcloud.css` into the Custom CSS Field at the Bottom.
Just hit Save and you're done!